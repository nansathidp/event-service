package service

import (
	"fmt"
	"gitlab.com/nansathidp/event/schedule"
	"gitlab.com/nansathidp/event/state"
	"gitlab.com/nansathidp/event/task"
	"google.golang.org/grpc"
	"log"
	"net"
	"os"
	"time"
)

type Service interface {
	Listening()
}

type ServerService struct {
}

type TaskService struct {
}

func NewService(name string) Service {
	if name == "server" {
		return &ServerService{}
	} else if name == "task" {
		return &TaskService{}
	}
	return nil
}

func (*ServerService) Listening() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "50051"
	}
	if lis, err := net.Listen("tcp", `:`+port); err != nil {
		log.Println(err)
		os.Exit(1)
	} else {
		server := grpc.NewServer()
		register(server)
		log.Println(fmt.Sprintf("Service listening  0.0.0.0:%s", port))
		if err := server.Serve(lis); err != nil {
			log.Println(err)
			os.Exit(1)
		}

	}
}

func (tasks *TaskService) Listening() {
	ticker := time.NewTicker(time.Second)
	quit := make(chan struct{})
	go func() {
		for {
			select {
			case <-quit:
				ticker.Stop()
				return
			}

		}
	}()
	for t := range ticker.C {
		fmt.Println(t, "Task fetch")
		task.Publish()
	}

}

func register(server *grpc.Server) {
	schedule.Register(server)
	state.Register(server)
}

