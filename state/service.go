package state

import (
	"context"
	"gitlab.com/nansathidp/event/model"
	"google.golang.org/grpc"
	"log"
)

type Service struct {
}

func (state *Service) SendState(ctx context.Context, body *Body) (*Response, error) {
	var schedule *model.EventSchedule
	if body.Key != "" {
		schedule = model.NewSchedule(body.Key)
	} else {
		schedule = model.NewSchedule()
	}
	schedule.SetPublish(body.Namespace, body.Topic, body.Preload)
	schedule.ToInit()
	log.Println("send-state:", body.Namespace, body.Topic)
	return &Response{Key: schedule.Key, Message: "Send success"}, nil
}

func Register(server *grpc.Server) {
	RegisterStateServiceServer(server, &Service{})
}
