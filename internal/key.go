package internal

import "github.com/google/uuid"

func GenerateKey() string {
	uuStr, _ := uuid.NewRandom()
	return uuStr.String()
}
