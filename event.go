package main

import (
	"github.com/spf13/cobra"
	"gitlab.com/nansathidp/event/driver"
	"gitlab.com/nansathidp/event/service"
	"log"
	"os"
)

func init() {
	driver.Listening()
}

var rootCmd = &cobra.Command{
	Use:   "event",
	Short: "Serve event of Applications",
	Long:  `Event is a service to another  applications.`,
	Run: func(cmd *cobra.Command, args []string) {
		instance := service.NewService(args[0])
		if instance == nil {
			log.Println("Service is not found.")
			os.Exit(0)
		}
		instance.Listening()

	},
}

func main() {

	if err := rootCmd.Execute(); err != nil {
		log.Print(err)
		os.Exit(1)
	}
}
