package model

import (
	"encoding/json"
	"fmt"
	"github.com/Kamva/mgm/v3"
	"gitlab.com/nansathidp/event/internal"
	"go.mongodb.org/mongo-driver/bson"
	//"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"time"
)

type EventSchedule struct {
	mgm.DefaultModel `bson:",inline"`
	Key              string    `json:"key" bson:"key"`
	Topic            string    `json:"topic" bson:"topic"`
	RunAt            time.Time `json:"run_at" bson:"runAt"`
	Preload          string    `json:"preload" bson:"preload"`
	Namespace        string    `json:"namespace" bson:"namespace"`
	Backtrack        []string  `json:"backtrack" bson:"backtrack"`
	Status           string    `json:"status" bson:"status"`
	IsTry            bool      `json:"is_try" bson:"isTry"`
	Count            int64     `json:"runCount" bson:"runCount"`
}

func (*EventSchedule) CollectionName() string {
	return "schedule_publish"
}

func NewSchedule(keys ...string) *EventSchedule {
	if len(keys) > 0 {
		return &EventSchedule{Key: keys[0]}
	}
	return &EventSchedule{Key: internal.GenerateKey()}
}

func (eventSchedule *EventSchedule) SetSchedule(topic string, runAt time.Time, preload string, namespace string) {
	eventSchedule.Preload = preload
	eventSchedule.Topic = topic
	eventSchedule.RunAt = runAt
	eventSchedule.Namespace = namespace

}

func (eventSchedule *EventSchedule) ToInit() {
	eventSchedule.Status = "init"
	eventSchedule.Save()
}

func (eventSchedule *EventSchedule) ToWait() {
	eventSchedule.Status = "wait"
	eventSchedule.Save()
}

func (eventSchedule *EventSchedule) SetPublish(namespace string, topic string, preload string) {
	eventSchedule.Preload = preload
	eventSchedule.Namespace = namespace
	eventSchedule.Topic = topic
	eventSchedule.RunAt = time.Now()

}

func (eventSchedule *EventSchedule) Cancel() error {
	temp := &EventSchedule{}
	if err := mgm.Coll(eventSchedule).First(bson.M{"key": eventSchedule.Key}, temp); err != nil {
		fmt.Sprintf("Event key %s is null\n", eventSchedule.Key)
		return nil
	}
	if err := mgm.Coll(temp).Delete(temp); err != nil {
		return err
	}
	log.Println("cancel schedule: ", eventSchedule.Key)
	return nil
}

func (eventSchedule *EventSchedule) ToPreload() interface{} {
	var preload interface{}
	if err := json.Unmarshal([]byte(eventSchedule.Preload), &preload); err != nil {
		return nil
	}
	return preload
}
func (eventSchedule *EventSchedule) Save() *EventSchedule {
	temp := &EventSchedule{}
	if err := mgm.Coll(eventSchedule).First(bson.M{"key": eventSchedule.Key}, temp); err != nil {
		if err := mgm.Coll(eventSchedule).Create(eventSchedule); err != nil {
			log.Println(err, "error")
			return nil
		}

	} else {

		temp.Topic = eventSchedule.Topic
		temp.Key = eventSchedule.Key
		temp.Preload = eventSchedule.Preload
		temp.RunAt = eventSchedule.RunAt
		temp.Backtrack = eventSchedule.Backtrack
		temp.IsTry = eventSchedule.IsTry
		temp.Status = eventSchedule.Status
		temp.Count = eventSchedule.Count
		temp.Namespace = eventSchedule.Namespace

		if err := mgm.Coll(temp).Update(temp); err != nil {
			log.Println(err)
			return nil
		}
	}
	return eventSchedule
}

func GetTaskAll() []*EventSchedule {
	filter := bson.D{
		{"runAt", bson.D{{"$lte", time.Now().UTC()}}},
		{"status", "init"},
	}
	var items []*EventSchedule
	eventCollection := mgm.Coll(&EventSchedule{})
	if err := eventCollection.SimpleFind(&items, filter); err != nil {
		log.Println(err, "all")
	}
	return items
}

func GetTask() *EventSchedule {
	eventCollection := mgm.Coll(&EventSchedule{})
	item := &EventSchedule{}
	filter := bson.D{
		{"runAt", bson.D{{"$lte", time.Now().UTC()}}},
		{"status", "init"},
	}
	if err := eventCollection.First(filter, item); err != nil {
		log.Println(err, "all")
	}
	return item
}

func (eventSchedule *EventSchedule) ToError(err error) {
	eventSchedule.Backtrack = append(eventSchedule.Backtrack, err.Error())
	eventSchedule.IsTry = true
	eventSchedule.Count += 1
	eventSchedule.Save()
}

func (eventSchedule *EventSchedule) ToSuccess() {
	if err := mgm.Coll(eventSchedule).Delete(eventSchedule); err != nil {
		log.Println(err)
	} else {
		NewInventory(eventSchedule)
	}
}

func (eventSchedule *EventSchedule) IsExecute() bool {
	return eventSchedule.Status == "init" || eventSchedule.IsTry
}

func (eventSchedule *EventSchedule) IsClean() bool {
	return eventSchedule.Count > 3
}
