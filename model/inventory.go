package model

import (
	"github.com/Kamva/mgm/v3"
	"log"
)

type Inventory struct {
	mgm.DefaultModel `bson:",inline"`
	TransactionKey   string `json:"transactionKey"`
	Namespace        string `json:"namespace" bson:"namespace"`
	Topic            string `json:"topic" bson:"topic"`
	Preload          string `json:"preload"`
}

func NewInventory(schedule *EventSchedule) {

	if err := mgm.Coll(&Inventory{}).Create(&Inventory{
		TransactionKey: schedule.Key,
		Namespace:      schedule.Namespace,
		Topic:          schedule.Topic,
		Preload:        schedule.Preload,
	}); err != nil {
		log.Println(err)
	}
}

func (*Inventory) CollectionName() string {
	return "schedule_inventory"
}
