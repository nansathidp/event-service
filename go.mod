module gitlab.com/nansathidp/event

go 1.14

require (
	github.com/Kamva/mgm/v3 v3.0.1
	github.com/golang/protobuf v1.4.2
	github.com/google/uuid v1.1.2
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/spf13/cobra v0.0.3
	github.com/streadway/amqp v1.0.0
	go.mongodb.org/mongo-driver v1.4.1
	google.golang.org/grpc v1.32.0
)
