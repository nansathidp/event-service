package schedule

import (
	"context"
	"gitlab.com/nansathidp/event/model"
	"google.golang.org/grpc"
	"log"
	"time"
)

type Service struct {
}

func (*Service) SendScheduler(ctx context.Context, body *Body) (*Response, error) {
	var schedule *model.EventSchedule
	if body.Key != "" {
		schedule = model.NewSchedule(body.Key)
	} else {
		schedule = model.NewSchedule()
	}
	runAt := time.Unix(body.RunAt, 0)
	schedule.SetSchedule(body.Topic, runAt, body.Preload, body.Namespace)
	log.Println("send-schedule: ", runAt, body.Preload)
	schedule.ToInit()
	return &Response{Key: schedule.Key, Message: "Schedule init success"}, nil

}
func (*Service) DeleteSchedule(ctx context.Context, token *Token) (*Response, error) {
	schedule := model.NewSchedule(token.Key)
	return &Response{Key: token.Key, Message: "delete schedule"}, schedule.Cancel()
}

func Register(server *grpc.Server) {
	RegisterPublishScheduleServiceServer(server, &Service{})
}
