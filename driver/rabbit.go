package driver

import (
	"fmt"
	"github.com/streadway/amqp"
	"log"
	"os"
)

func NewMQ() *amqp.Connection {
	conn, err := amqp.Dial(MQUri())
	failOnError(err, "Failed to connect to RabbitMQ")
	return conn
}

func MQUri() string {
	return fmt.Sprintf(
		"amqp://%s:%s@%s:%s/",
		os.Getenv("MQ_USER"),
		os.Getenv("MQ_PASS"),
		os.Getenv("MQ_HOST"),
		os.Getenv("MQ_PORT"),
	)
}
func failOnError(e error, s string) {
	if e != nil {
		log.Println(s, e)
		os.Exit(1)
	}
}
