package driver

import (
	"fmt"
	"github.com/Kamva/mgm/v3"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"os"
)


func Listening() {
	log.Println(uri())
	if err := mgm.SetDefaultConfig(nil, os.Getenv("MONGO_NAME"), options.Client().ApplyURI(uri())); err != nil {
		log.Println(err)
		os.Exit(1)
	} else {
		log.Println("Mongodb connection")

	}
}

func uri() string {
	return fmt.Sprintf("mongodb://%s:%s@%s:%s/%s?authSource=admin",
		os.Getenv("MONGO_USER"),
		os.Getenv("MONGO_PASS"),
		os.Getenv("MONGO_HOST"),
		os.Getenv("MONGO_PORT"),
		os.Getenv("MONGO_NAME"),
	)
}
