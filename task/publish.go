package task

import (
	"encoding/json"
	"fmt"
	"github.com/streadway/amqp"
	"gitlab.com/nansathidp/event/driver"
	"gitlab.com/nansathidp/event/model"
	"log"
)

type PreloadPublish struct {
	Namespace string      `json:"namespace"`
	Topic     string      `json:"topic"`
	Key       string      `json:"key"`
	Preload   interface{} `json:"preload"`
}

func NewPublish(schema *model.EventSchedule) *PreloadPublish {
	return &PreloadPublish{Topic: schema.Topic, Namespace: schema.Namespace, Key: schema.Key, Preload: schema.ToPreload()}
}

func (preloadPublish *PreloadPublish) topicName() string {
	return fmt.Sprintf("%s.%s", preloadPublish.Namespace, preloadPublish.Topic)
}

func (preloadPublish *PreloadPublish) send() error {
	client := driver.NewMQ()
	channel, err := client.Channel()
	if err != nil {
		return err
	}
	err = channel.ExchangeDeclare(
		preloadPublish.topicName(), // name
		"topic",                    // type
		true,                       // durable
		false,                      // auto-deleted
		false,                      // internal
		false,                      // no-wait
		nil,                        // arguments
	)
	defer channel.Close()
	defer client.Close()
	if body, err := json.Marshal(preloadPublish); err != nil {
		return err
	} else {
		log.Println("Publish ---> : ", preloadPublish.topicName())
		return channel.Publish(preloadPublish.topicName(), "", false, false, amqp.Publishing{Body: body, ContentType: "application/json"})
	}
}

func Publish() {
	item := model.GetTask()
	if item != nil {
		publish := NewPublish(item)
		publish.commit(item)
	}
	//for _, item := range model.GetTaskAll() {
	//
	//}
}

func (preloadPublish *PreloadPublish) commit(meta *model.EventSchedule) {
	if meta.IsExecute() {
		if err := preloadPublish.send(); err != nil {
			meta.ToError(err)
		} else {
			meta.ToSuccess()
		}
	}
}
