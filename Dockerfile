FROM golang:1.14 as builder
WORKDIR /app/
COPY . .
RUN GO111MODULE=on CGO_ENABLED=0 GOOS=linux go build -o event

FROM alpine:latest
WORKDIR /app
COPY --from=builder /app/event /bin/event
